;;; Requires an ASDF version with package-inferred-system
(cl:unless (asdf:version-satisfies (asdf:asdf-version) "3.1.2")
  (cl:error "Big FOSS requires ASDF 3.1.2 or later."))

(asdf:defsystem #:big-foss
  :description "A new Common Lisp IRC client."
  :version "0.0.0.0"
  :author "Michael Babich"
  :maintainer "Michael Babich"
  :license "MIT"
  :homepage "https://bigfoss.net/"
  :bug-tracker "https://gitlab.com/big-foss/big-foss/issues"
  :source-control (:git "https://gitlab.com/big-foss/big-foss.git")
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on (:alexandria
               :bordeaux-threads
               :uiop
               :usocket
               :zr-irc
               :zr-utils
               :big-foss/all)
  ; :in-order-to ((asdf:test-op (asdf:test-op "big-foss/tests")))
  )
