BIG FOSS
========

Big FOSS is an upcoming IRC client, written in Common Lisp. The FOSS
in the name refers to free and open source software (FOSS), while the
"big" part of its name represents its ambitious desire to be the
future of online networking, building on top of the Internet Relay
Chat (IRC) protocol and the `zr-irc` library in particular.

Big FOSS will be a terminal-style IRC client, built on top of a
pseudoterminal inside of the Zombie Raptor Common Lisp game engine,
giving it advanced graphical capabilities to allow for the integration
of modern features while still retaining the classic/retro feel of
IRC. Since it will be unable to run in `tmux` or `screen`, unlike a
true in-terminal IRC client, it will come with its own IRC "bouncer"
software to maintain a persistent connection to IRC networks.

More information will be coming soon.

Feel free to [fork Big FOSS on
Gitlab](https://gitlab.com/big-foss/big-foss) to contribute soon!
